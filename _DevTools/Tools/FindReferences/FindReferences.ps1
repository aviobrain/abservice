#-------------------------------------
# FindReferences 1.0.0.1
# Copyright (c) 2013 Davide Gironi
#
# Please refer to LICENSE file for licensing information.
#-------------------------------------

param (
	[string] $baseDir = ".\",
	[string] $fileFindName = "",
	[bool] $isBinaryFile = $false
)

$baseDir = Resolve-Path .\$baseDir

Write-Host
Write-Host -ForegroundColor Green "Folder to search in " $baseDir
Write-Host -ForegroundColor Green "FileName to search " $fileFindName
Write-Host

#Get-ChildItem -Path V:\Myfolder -Filter CopyForbuild.bat -Recurse
$count = 0
Get-ChildItem -Path $baseDir -Filter $fileFindName -Recurse  | ForEach-Object {
	if($_.GetType() -eq [System.IO.FileInfo])
	{
		#get relative directory path
		$directory = $_.Directory.ToString().Replace($baseDir + "\", "")
		
		#check only for projects library and source directories
		if(!$directory.StartsWith("_") -and !$directory.Contains("\bin\") -and !$directory.Contains("\obj\") -and $directory.Contains("\packages\"))
		{
			$count++
			
			$versionFindPattern = 'Version: [0-9]+(\.([0-9]+|\*)){1,3}'
			
			#get file name
			$filename = $_.Name
			$fullfilename = $_.Directory.ToString() + '\' + $_.Name
			
			#get version
			$versionValue = ""
			try
			{
				if ($isBinaryFile -eq $true)
				{
					$versionValue = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($fullfilename).FileVersion
				}
				else
				{
					Get-Content -Path $fullfilename | %{ [Regex]::Matches($_, $versionFindPattern) } | %{ $versionValue = $_.Value.toString().Replace("Version: ", "") }
				}
			}
			catch {}
			
			#write output
			Write-Host -ForegroundColor Green $count ")"
			Write-Host "   Directory    : " $directory
			Write-Host "   FullFileName : " $directory"\"$filename
			Write-Host "   Version      : " $versionValue
		}
	}
}
Write-Host
 
Write-Host -ForegroundColor Green "Found " $count " items"

Write-Host
