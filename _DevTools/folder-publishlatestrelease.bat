@echo off

rem Copy the latest Release to the a folder
rem Copyright (c) Davide Gironi, 2014

rem load config
if not exist config.folder-publishlatestrelease.bat exit
call config.folder-publishlatestrelease.bat

rem check base destination path
if not exist %DESTBASEPATH% exit

rem set solution name as parent folder
for %%* in (../.) do set SOLUTIONNAME=%%~nx*

rem add solution name to destination folder name
set DESTPATH=%DESTBASEPATH%\%SOLUTIONNAME%

rem search the newest release folder
set NEWESTRELEASEDIR=
for /f "delims=" %%a in ('dir /b /ad-h /t:c /od "%RELEASEDIR%"') do (
	set NEWESTRELEASEDIR=%%a
)

if not [%NEWESTRELEASEDIR%] == [] (
	rem make destination path
	if not exist %DESTPATH% mkdir %DESTPATH%

	rem mirror the destination path to current folder
	robocopy "%RELEASEDIR%\%NEWESTRELEASEDIR%" "%DESTPATH%" /MIR /FFT
)
