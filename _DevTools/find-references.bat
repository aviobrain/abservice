@echo off

setlocal enabledelayedexpansion

rem load config
if not exist config.find-references.bat exit
call config.find-references.bat

call :parse "%NAMES%"
pause
goto :eof

:parse
setlocal
set LIST=%~1
for /F "tokens=1* delims=," %%f in ("%LIST%") do (
    rem if the item exist
    if not "%%f" == "" (
		if "%%~xf" == ".dll" (
			powershell -Command "& { [Console]::WindowWidth = 150; [Console]::WindowHeight = 50; .\Tools\FindReferences\FindReferences.ps1 -baseDir '%BASEPATH%' -fileFindName '%%f' -isBinaryFile $true; }"
		) else if "%%~xf" == ".exe" (
			powershell -Command "& { [Console]::WindowWidth = 150; [Console]::WindowHeight = 50; .\Tools\FindReferences\FindReferences.ps1 -baseDir '%BASEPATH%' -fileFindName '%%f' -isBinaryFile $true; }"
		) else (
			powershell -Command "& { [Console]::WindowWidth = 150; [Console]::WindowHeight = 50; .\Tools\FindReferences\FindReferences.ps1 -baseDir '%BASEPATH%' -fileFindName '%%f' -isBinaryFile $false; }"
		)
	)
    rem if next item exist
    if not "%%g" == "" call :parse "%%g"
)
endlocal
goto :eof
