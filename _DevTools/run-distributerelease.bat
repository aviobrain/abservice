@echo off

rem Distribute the last Release for a solution to selected folders
rem Copyright (c) Davide Gironi, 2015

rem load config
if not exist config.run-distributerelease.bat exit
call config.run-distributerelease.bat

set ZIP="Tools\7-zip\7za.exe"
set DISTRIBUTIONDIRPATH=%DISTRIBUTIONDIR%packages\Int

set NEWESTRELEASE=
set NEWESTRELEASEDIR=
for /f "delims=" %%a in ('dir /b /ad-h /t:c /od "%RELEASEDIR%"') do (
	set NEWESTRELEASEDIR=%%a
)
if not ["%NEWESTRELEASEDIR%"] == [] (
	for /f "tokens=*" %%b in ('dir /b /od "%RELEASEDIR%\%NEWESTRELEASEDIR%\*-%1*.zip"') do (
		set NEWESTRELEASE=%RELEASEDIR%\%NEWESTRELEASEDIR%\%%b
	)
)

if not ["%NEWESTRELEASE%"] == [] (
	echo Latest Release %NEWESTRELEASE%
	for %%p in ("%PROJECTS:,=" "%") do (
		echo %DISTRIBUTIONDIRPATH%\%%~p.distributionlist.txt
		if exist "%DISTRIBUTIONDIRPATH%\%%~p.distributionlist.txt" (
			echo Reading "%DISTRIBUTIONDIRPATH%\%%~p.distributionlist.txt"
			for /f "tokens=* usebackq" %%l in ("%DISTRIBUTIONDIRPATH%\%%~p.distributionlist.txt") do (
				echo Delete destination folder "%DISTRIBUTIONDIRPATH%\%%l\%%~p"
				rmdir "%DISTRIBUTIONDIRPATH%\%%l\%%~p" /S /Q
				echo Extract project "%%~p" to "%DISTRIBUTIONDIRPATH%\%%l\%%~p" from "%NEWESTRELEASE%"
				%ZIP% x "%NEWESTRELEASE%" -o"%DISTRIBUTIONDIRPATH%\%%l" "%%~p" -r -y
			)
		) else (
			echo Not found "%DISTRIBUTIONDIRPATH%\%%~p.distributionlist.txt", building an empty file
			echo. 2> "%DISTRIBUTIONDIRPATH%\%%~p.distributionlist.txt"
		)
	)
)

exit
