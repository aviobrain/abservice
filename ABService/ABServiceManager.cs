﻿#region License
// Copyright (c) 2013 AvioBrain
//                    Davide Gironi
//
// Please refer to LICENSE file for licensing information.
#endregion

using System;
using System.Timers;

namespace AB.Service
{
    public class ABServiceManager
    {
        //service timer
        private readonly Timer _timerService = new Timer();
        //runnow timer
        private readonly Timer _timerRunNow = new Timer();

        //_action to run
        private readonly Action _action;

        //service status
        public enum ServiceStatus { Start, Stop };
        //running Action status
        public enum ActionStatus { Running, NotRunning };

        //current Service status
        private ServiceStatus _currentServiceStatus = ServiceStatus.Stop;
        //current Action status
        private ActionStatus _currentActionStatus = ActionStatus.NotRunning;

        //soft stop the serice command
        public const int SoftStop = 130;

        private bool _serviceEnabled = true; //private reference to the Service enabled
        /// <summary>
        /// get or set the Service enabled status
        /// </summary>
        public bool ServiceEnabled
        {
            get { return _serviceEnabled; }
            set { _serviceEnabled = value; }
        }

        /// <summary>
        /// Initialize the Service Manager
        /// </summary>
        /// <param name="secondsLaunchInterval"></param>
        /// <param name="action"></param>
        public ABServiceManager(int secondsLaunchInterval, Action action)
        {
            _action = action;

            _timerService.Interval = 1000 * secondsLaunchInterval; //interval in seconds
            _timerService.Enabled = true;
            _timerService.Elapsed += new ElapsedEventHandler(ServiceFunc);

            _timerRunNow.Interval = 1000 * 1; //1 sec
            _timerRunNow.Enabled = true;
            _timerRunNow.Elapsed += new ElapsedEventHandler(ServiceFunc);

            SetServiceStatus(ServiceStatus.Stop);
        }

        /// <summary>
        /// Get the Service Status
        /// </summary>
        /// <returns></returns>
        public ServiceStatus GetServiceStatus()
        {
            return _currentServiceStatus;
        }

        /// <summary>
        /// Get the Service Status string value
        /// </summary>
        /// <returns></returns>
        public string GetServiceStatusToString()
        {
            if (_currentServiceStatus == ServiceStatus.Start)
                if (_currentActionStatus == ActionStatus.Running)
                    return "started & running";
                else
                    return "started & not running";
            else if (_currentServiceStatus == ServiceStatus.Stop)
                if (_currentActionStatus == ActionStatus.Running)
                    return "stopped & running";
                else
                    return "stopped & not running";
            else
                return "error";
        }

        /// <summary>
        /// Set the Service Status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool SetServiceStatus(ServiceStatus status)
        {
            bool ret = true;

            switch (status)
            {
                case ServiceStatus.Start:
                    if (_currentServiceStatus == ServiceStatus.Stop)
                    {
                        //start the service
                        _currentServiceStatus = ServiceStatus.Start;
                        _timerService.Start();
                        _timerRunNow.Start();
                        ret = true;
                    }
                    break;
                case ServiceStatus.Stop:
                    if (_currentServiceStatus == ServiceStatus.Start)
                    {
                        //stop the service
                        _currentServiceStatus = ServiceStatus.Stop;
                        _timerService.Stop();
                        _timerRunNow.Stop();
                        ret = true;
                    }
                    break;
                default:
                    ret = false;
                    break;
            }

            return ret;
        }

        /// <summary>
        /// Run the Service now
        /// </summary>
        /// <returns></returns>
        public bool RunNow()
        {
            bool ret = false;

            if (_currentServiceStatus == ServiceStatus.Start && _currentActionStatus != ActionStatus.NotRunning)
            {
                _timerRunNow.Start();
                ret = true;
            }

            return ret;
        }

        /// <summary>
        /// get the _action status for this service
        /// </summary>
        /// <returns></returns>
        public ActionStatus GetActionStatus()
        {
            return _currentActionStatus;
        }

        /// <summary>
        /// Service Action runner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServiceFunc(object sender, EventArgs e)
        {
            _timerRunNow.Stop(); //stop timer if we are running a runnow instance

            //run the current _action only if we are not already running it
            if (_currentActionStatus != ActionStatus.Running)
            {
                _currentActionStatus = ActionStatus.Running;
                if (_serviceEnabled)
                {
                    _action();
                }
                _currentActionStatus = ActionStatus.NotRunning;
            }
        }

    }
}
