﻿#region License
// Copyright (c) 2013 AvioBrain
//                    Davide Gironi
//
// Please refer to LICENSE file for licensing information.
#endregion

using System.ComponentModel;
using System.ServiceProcess;

namespace AB.Service
{
    [RunInstaller(true)]
    public class ABServiceInstaller : System.Configuration.Install.Installer
    {
        /// <summary>
        /// Initialize the Installer
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="serviceDisplayName"></param>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceStartMode"></param>
        protected void InitInstaller(string serviceName, string serviceDisplayName, string serviceDescription, ServiceStartMode serviceStartMode)
        {
            ServiceProcessInstaller installer = new ServiceProcessInstaller();
            installer.Account = ServiceAccount.LocalService;
            installer.Password = null;
            installer.Username = null;
            base.Installers.Add(installer);

            ServiceInstaller serviceInstaller = new ServiceInstaller();
            serviceInstaller.StartType = serviceStartMode;
            serviceInstaller.ServiceName = serviceName;
            serviceInstaller.DisplayName = serviceDisplayName;
            serviceInstaller.Description = serviceDescription;
            base.Installers.Add(serviceInstaller);
        }
        protected void InitInstaller(string serviceName, ServiceStartMode serviceStartMode)
        {
            InitInstaller(serviceName, serviceName, serviceName + " Service", serviceStartMode);
        }
        protected void InitInstaller(string serviceName)
        {
            InitInstaller(serviceName, serviceName, serviceName + " Service", ServiceStartMode.Automatic);
        }

    }
}
