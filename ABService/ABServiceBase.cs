﻿#region License
// Copyright (c) 2013 AvioBrain
//                    Davide Gironi
//
// Please refer to LICENSE file for licensing information.
#endregion

using System.ServiceProcess;

namespace AB.Service
{
    public class ABServiceBase : ServiceBase
    {

        private ABServiceManager _serviceManager = null; //private reference to the Service Manager
        /// <summary>
        /// get or set the Service Manager
        /// </summary>
        public ABServiceManager ServiceManager
        {
            get { return _serviceManager; }
            set { _serviceManager = value; }
        }

        /// <summary>
        /// call the ServiceBase OnStart method
        /// </summary>
        public void RunStart()
        {
            OnStart(null);
        }

        /// <summary>
        /// call the ServiceBase OnStop method
        /// </summary>
        public void RunStop()
        {
            OnStop();
        }

        /// <summary>
        /// wrapper for ServiceManager RunNow
        /// </summary>
        /// <returns></returns>
        public bool RunNow()
        {
            return _serviceManager.RunNow();
        }

        /// <summary>
        /// wrapper for ServiceManager GetServiceStatus
        /// </summary>
        /// <returns></returns>
        public ABServiceManager.ServiceStatus GetServiceStatus()
        {
            return _serviceManager.GetServiceStatus();
        }

        /// <summary>
        /// wrapper for ServiceManager GetServiceStatusToString
        /// </summary>
        /// <returns></returns>
        public string GetServiceStatusToString()
        {
            return _serviceManager.GetServiceStatusToString();
        }

        /// <summary>
        /// wrapper for ServiceManager GetActionStatus
        /// </summary>
        /// <returns></returns>
        public ABServiceManager.ActionStatus GetActionStatus()
        {
            return _serviceManager.GetActionStatus();
        }

        /// <summary>
        /// implements OnStart ServiceBase
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            _serviceManager.SetServiceStatus(ABServiceManager.ServiceStatus.Start);
        }

        /// <summary>
        /// implements OnStop ServiceBase
        /// </summary>
        protected override void OnStop()
        {
            _serviceManager.SetServiceStatus(ABServiceManager.ServiceStatus.Stop);
        }

        /// <summary>
        /// implements OnCustomCommand ServiceBase
        /// </summary>
        /// <param name="command"></param>
        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);

            if (command == ABServiceManager.SoftStop)
            {
                OnStop();

                while (_serviceManager.GetActionStatus() == ABServiceManager.ActionStatus.Running) { };
                this.Stop();
            }
        }
    }
}
