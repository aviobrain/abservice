﻿#region License
// Copyright (c) 2013 AvioBrain
//                    Davide Gironi
//
// Please refer to LICENSE file for licensing information.
#endregion

using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace AB.Service
{
    public static class ABServiceConsole
    {
        [DllImport("kernel32")]
        static extern bool AllocConsole();

        /// <summary>
        /// Run the Console Application
        /// </summary>
        /// <param name="args"></param>
        /// <param name="serviceLocation"></param>
        /// <param name="serviceBase"></param>
        public static void ConsoleApp(string[] args, string serviceLocation, ABServiceBase serviceBase)
        {
            if (System.Environment.UserInteractive)
            {
                //autoinstaller and console debug service
                if (args.Length > 0)
                {
                    //run this service in console mode
                    AllocConsole(); //allocate a console

                    switch (args[0])
                    {
                        case "--install":
                            //auto-install service
                            serviceBase.ServiceManager.ServiceEnabled = false;
                            try
                            {
                                System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { serviceLocation });
                            }
                            catch { }
                            break;
                        case "--uninstall":
                            //aute-remove service
                            serviceBase.ServiceManager.ServiceEnabled = false;
                            try
                            {
                                System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "/u", serviceLocation });
                            }
                            catch { }
                            break;
                        case "--run":
                            //run this service in console mode

                            //start service
                            serviceBase.RunStart();

                            Console.WriteLine("Press CTRL+C to Stop Service Console Mode.");
                            bool stoprunning = false;
                            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e)
                            {
                                e.Cancel = true;
                                stoprunning = true;
                                Console.WriteLine("Stopping Service Console Mode...");
                            };
                            //print this message while running
                            while (!stoprunning)
                            {
                                Console.WriteLine("Service Status: " + serviceBase.GetServiceStatusToString());
                                Thread.Sleep(1000 * 10);
                            }

                            //stop service
                            serviceBase.RunStop();
                            break;
                    }
                }

                Environment.Exit(0);
            }
        }
    }
}
