About
===

**ABService** is a *.NET Windows Service Application* helper, it will simplify the process of creating services on .NET.

## Download

+ **[NuGet](https://www.nuget.org/packages/AB.Service)**
+ **[Releases](https://bitbucket.org/aviobrain/abservice/downloads?tab=downloads)**

## Requirements

* Microsoft Windows with .NET framework 4.5 or later

## Development

If you want to contribute, or you found a bug, please send an e-mail to the software author.

## License

Copyright (c) Davide Gironi, 2013  
ABService is an open source software licensed under the [GPLv3 license](http://opensource.org/licenses/GPL-3.0)
