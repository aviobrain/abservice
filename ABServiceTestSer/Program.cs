﻿using System.ServiceProcess;

namespace AB.ServiceTest.Service.Test
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (System.Environment.UserInteractive)
                AB.Service.ABServiceConsole.ConsoleApp(args, System.Reflection.Assembly.GetExecutingAssembly().Location, new ServiceMain());

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ServiceMain()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
