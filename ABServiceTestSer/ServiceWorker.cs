﻿using AB.Service;
using NCrontab;
using System;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace AB.ServiceTest.Service.Test
{
    public class ServiceWorker
    {
        private log4net.ILog log = null;

        private DateTime nextOccurrence = DateTime.Now;

        public ServiceWorker()
        {
            //set current working directory and load logger files
            Directory.SetCurrentDirectory(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
            log4net.Config.XmlConfigurator.Configure(new FileInfo("Config.log4net"));
            log = log4net.LogManager.GetLogger("MainLogger");
        }

        public void Do()
        {
            Console.WriteLine("sec");
            if (nextOccurrence < DateTime.Now || nextOccurrence == null)
            {
                CrontabSchedule c = CrontabSchedule.Parse(ConfigurationManager.AppSettings["CronTab"]);
                nextOccurrence = c.GetNextOccurrence(DateTime.Now, DateTime.Now.AddMinutes(1));

                log.Info("starting...");
                log.Info("  doing nothing for 60 seconds");
                Thread.Sleep(60000);
                log.Info("stopping...");
            }
        }

        private void StopServiceSampleCommand()
        {
            new ServiceController(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name).ExecuteCommand(ABServiceManager.SoftStop);
        }

    }
}
