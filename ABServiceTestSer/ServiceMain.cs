﻿namespace AB.ServiceTest.Service.Test
{
    public partial class ServiceMain : AB.Service.ABServiceBase
    {
        public ServiceMain()
        {
            ServiceWorker serviceWorker = new ServiceWorker();
            ServiceManager = new AB.Service.ABServiceManager(1, () => serviceWorker.Do());

            InitializeComponent();
        }
    }
}
