﻿using System.ComponentModel;

namespace AB.ServiceTest.Service.Test
{
    [RunInstaller(true)]
    public partial class InstallerMain : AB.Service.ABServiceInstaller
    {
        public InstallerMain()
        {
            InitInstaller(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString());

            InitializeComponent();
        }
    }
}
